# Dockerfile kali-light

# Official base image
FROM "docker.io/kalilinux/kali-rolling"

# Apt
RUN apt-get -y update && apt-get -y upgrade && apt-get -y autoremove && apt-get -y clean

# Tools
RUN apt-get install aircrack-ng bpytop crackmapexec crunch curl dirb dirbuster dnsenum dnsrecon dnsutils dos2unix enum4linux exploitdb feroxbuster ftp fzf grc golang-go git gobuster hashcat hping3 hydra impacket-scripts john joomscan masscan metasploit-framework mimikatz nasm ncat netcat-traditional nikto nmap npm nodejs openvpn oscanner patator php powersploit proxychains python3-impacket python3-pip python2 python3 recon-ng redis-tools responder samba samdump2 seclists smbclient smbmap snmp socat sqlmap ssh sslscan  theharvester tnscmd10g vim wafw00f weevely whatweb wfuzz whois wkhtmltopdf wordlists wpscan zsh -y --fix-missing

# Set working directory to /root
WORKDIR /root

# Make the main tools directory
RUN mkdir ~/Tools

# Installing Oh-My-ZSH
RUN  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
RUN git -C ~/.oh-my-zsh/custom/plugins/ clone https://github.com/zsh-users/zsh-autosuggestions && git -C ~/.oh-my-zsh/custom/plugins/ clone https://github.com/zsh-users/zsh-syntax-highlighting && git -C ~/.oh-my-zsh/custom/plugins/ clone https://github.com/zsh-users/zsh-completions
RUN cd ~/.oh-my-zsh/custom/plugins && git clone --depth 1 -- https://github.com/marlonrichert/zsh-autocomplete.git && cd ~/.oh-my-zsh/custom/plugins/ && git clone https://github.com/zlsun/solarized-man.git

# Passing my zshrc and vimrc to pod
RUN cd ~/ && wget https://gitlab.com/hog6fdym0uo/myzshrc/-/raw/6efa81973b257afb260fe99411535f26b7f439ed/.vimrc && rm -rf ~/.zshrc && wget https://gitlab.com/hog6fdym0uo/myzshrc/-/raw/6efa81973b257afb260fe99411535f26b7f439ed/.zshrc

# Installing Project Discovery's Notify and pulling settings
RUN GO111MODULE=on go get -v github.com/projectdiscovery/notify/cmd/notify
RUN mkdir -p ~/.config/notify && cd ~/.config/notify && wget https://gitlab.com/hog6fdym0uo/myzshrc/-/raw/6efa81973b257afb260fe99411535f26b7f439ed/provider-config.yaml

# Autorecon installation
RUN git -C ~/Tools/ clone --branch beta https://github.com/Tib3rius/AutoRecon 
RUN cd ~/Tools/AutoRecon/ && python3 -m pip install -r requirements.txt
RUN ln -s ~/Tools/AutoRecon/src/autorecon/autorecon.py /usr/local/bin/autorecon

# Decompressing RockYou
RUN gunzip -d /usr/share/wordlists/rockyou.txt.gz

# Downloading and installing shellerator
RUN cd ~/Tools && git clone https://github.com/ShutdownRepo/shellerator && cd shellerator && python3 setup.py install --user

# Downloading PEASS Suite
RUN cd ~/Tools && git clone https://github.com/carlospolop/PEASS-ng 

# Downloading linux-exploit-suggester-2
RUN cd ~/Tools && git clone https://github.com/jondonas/linux-exploit-suggester-2

# Downloading pspy64
RUN cd ~/Tools && mkdir /opt/pspy && wget https://github.com/DominicBreuker/pspy/releases/download/v1.2.0/pspy64

# Downloading OneListForAll Wordlist
RUN cd /usr/share/wordlists && wget https://github.com/six2dez/OneListForAll/releases/download/v2.3/onelistforall.txt && wget https://github.com/six2dez/OneListForAll/releases/download/v2.3/onelistforallmicro.txt && wget https://github.com/six2dez/OneListForAll/releases/download/v2.3/onelistforallshort.txt

# Installing lsd
RUN wget https://github.com/Peltoche/lsd/releases/download/0.20.1/lsd_0.20.1_amd64.deb && dpkg -i lsd_0.20.1_amd64.deb

# Installing Lynis Security Auditor
RUN cd ~/Tools && git clone https://github.com/CISOfy/lynis

# ReconFTW
RUN cd ~/Tools && git clone https://github.com/six2dez/reconftw && cd reconftw && ./install.sh
