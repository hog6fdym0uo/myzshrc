set number
inoremap ii <Esc>
cnoremap ii <C-C>
syntax on
color koehler
filetype on
filetype plugin indent on
set hidden
set wildmenu
set hlsearch
set wildmode=longest,list,full
set showcmd
set backspace=indent,eol,start
set autoindent
set notimeout ttimeout ttimeoutlen=200
set pastetoggle=<F11>
set confirm
set visualbell
set t_vb=
set mouse=a
set cmdheight=2
set relativenumber
set ignorecase
set smartcase
set linebreak
set history=1000
set background=dark
set clipboard=unnamed 


" Shortcut to use blackhole register by default
nnoremap d "_d
vnoremap d "_d
nnoremap D "_D
vnoremap D "_D
nnoremap c "_c
vnoremap c "_c
nnoremap C "_C
vnoremap C "_C
nnoremap x "_x
vnoremap x "_x
nnoremap X "_X
vnoremap X "_X

" Make presentation in sent:
map <F8> :w!<CR>:!sent <c-r>%<CR><CR>

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'christoomey/vim-system-copy'
Plug 'alaviss/nim.nvim'
Plug 'jeetsukumaran/vim-buffergator'
call plug#end()

let s:nimlspexecutable = "nimlsp"
let g:lsp_log_verbose = 1
let g:lsp_log_file = expand('/tmp/vim-lsp.log')
" for asyncomplete.vim log
let g:asyncomplete_log_file = expand('/tmp/asyncomplete.log')

inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? asyncomplete#close_popup() . "\<cr>" : "\<cr>"

let g:asyncomple_auto_popup = 0

inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ asyncomplete#force_refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

if has('win32')
   let s:nimlspexecutable = "nimlsp.cmd"
   " Windows has no /tmp directory, but has $TEMP environment variable
   let g:lsp_log_file = expand('$TEMP/vim-lsp.log')
   let g:asyncomplete_log_file = expand('$TEMP/asyncomplete.log')
endif
if executable(s:nimlspexecutable)
   au User lsp_setup call lsp#register_server({
   \ 'name': 'nimlsp',
   \ 'cmd': {server_info->[s:nimlspexecutable]},
   \ 'whitelist': ['nim'],
   \ })
endif

au User asyncomplete_setup call asyncomplete#register_source({
    \ 'name': 'nim',
    \ 'whitelist': ['nim'],
    \ 'completor': {opt, ctx -> nim#suggest#sug#GetAllCandidates({start, candidates -> asyncomplete#complete(opt['name'], ctx, start, candidates)})}
    \ })


function! AdjustFontSize(amount)
    if !has("gui_running")
        echoerr "You need to run a GUI version of Vim to use this function."
    endif

    let l:minfontsize = 6
    let l:maxfontsize = 16

    " Windows and macOS &guifont: Hack\ NF:h10:cANSI
    " Linux &guifont: Hack\ Nerd\ Font\ Mono\ Regular\ 10
    
    " A multiplatform pattern for Linux, Windows, and macOS:
    " \v            very magical.
    " (^\D{-1,})    Capture group 1 = [Anchored at the start of the string, match any character that is not [0-9] 1 or more times non-greedy].
    " (\d+)         Capture group 2 = [match [0-9] 1 or more times].
    " (\D*$)        Capture group 3 = [match any character that is not [0-9] 0 or more times to the end of the string].
    let l:pattern = '\v(^\D{-1,})(\d+)(\D*$)'

    " Break the font string into submatches.
    let l:matches = matchlist(&guifont, l:pattern)
    let l:start = l:matches[1]
    let l:size = l:matches[2]
    let l:end = l:matches[3]

    let newsize = l:size + a:amount
    if (newsize >= l:minfontsize) && (newsize <= l:maxfontsize)
        let newfont = l:start . newsize . l:end
        let &guifont = newfont
    endif
endfunction

nnoremap <silent> <F11> :call AdjustFontSize(-1)<CR>
inoremap <silent> <F11> <Esc>:call AdjustFontSize(-1)<CR>
vnoremap <silent> <F11> <Esc>:call AdjustFontSize(-1)<CR>
cnoremap <silent> <F11> <Esc>:call AdjustFontSize(-1)<CR>
onoremap <silent> <F11> <Esc>:call AdjustFontSize(-1)<CR>

nnoremap <silent> <F12> :call AdjustFontSize(1)<CR>
inoremap <silent> <F12> <Esc>:call AdjustFontSize(1)<CR>
vnoremap <silent> <F12> <Esc>:call AdjustFontSize(1)<CR>
cnoremap <silent> <F12> <Esc>:call AdjustFontSize(1)<CR>
onoremap <silent> <F12> <Esc>:call AdjustFontSize(1)<CR>

" Hold Control + scroll mouse-wheel to zoom text.
" NOTE: This event only works for Linux and macOS. SEE: :h scroll-mouse-wheel
map <silent> <C-ScrollWheelDown> :call AdjustFontSize(-1)<CR>
map <silent> <C-ScrollWheelUp> :call AdjustFontSize(1)<CR>

" CtrlP Fuzzy Finder and settings
set runtimepath^=~/.vim/bundle/ctrlp.vim

" Setup some default ignores
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/](\.(git|hg|svn)|\_site)$',
  \ 'file': '\v\.(exe|so|dll|class|png|jpg|jpeg)$',
\}

" Use the nearest .git directory as the cwd
" This makes a lot of sense if you are working on a project that is in version
" control. It also supports works with .svn, .hg, .bzr.
let g:ctrlp_working_path_mode = 'r'

let mapleader=","

" Use a leader instead of the actual named binding
nmap <leader>p :CtrlP<cr>

" Easy bindings for its various modes
nmap <leader>bb :CtrlPBuffer<cr>
nmap <leader>bm :CtrlPMixed<cr>
nmap <leader>bs :CtrlPMRU<cr>

" Buffergator options
" Use the right side of the screen
let g:buffergator_viewport_split_policy = 'R'

" I want my own keymappings...
let g:buffergator_suppress_keymaps = 1

" Looper buffers
"let g:buffergator_mru_cycle_loop = 1

" Go to the previous buffer open
nmap <leader>jj :BuffergatorMruCyclePrev<cr>

" Go to the next buffer open
nmap <leader>kk :BuffergatorMruCycleNext<cr>

" View the entire list of buffers open
nmap <leader>bl :BuffergatorOpen<cr>

" Shared bindings from Solution #1 from earlier
nmap <leader>T :enew<cr>
nmap <leader>bq :bp <BAR> bd #<cr>

" Persistent undo saves change history to disk
let s:undodir = "/tmp/.undodir_" . $USER
if !isdirectory(s:undodir)
    call mkdir(s:undodir, "", 0700)
endif
let &undodir=s:undodir
set undofile

" Signs in the number column
set signcolumn=number

" Show hidden files with CtrlP. This increases indexing time so default is off
let g:ctrlp_show_hidden = 1

let g:buffergator_display_regime='filepath'

autocmd VimLeave * call system("echo -n $'" . escape(getreg(), "'") . "' | xsel -ib")
